﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolRegistry
{
    public partial class Form1 : Form
    {
        DBConnect conn = new DBConnect();
        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = conn.viewData();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.Hide();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            dataGridView1.Hide();
            InsertForm add = new InsertForm();
            add.Visible = true;
            this.Hide();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            Application.Exit();
        }


        private void viewStudents(object sender, EventArgs e)
        {
            dataGridView1.Show();
            dataGridView1.DataSource = conn.viewData();
        }

        private void updateStudents(object sender, EventArgs e)
        {
            dataGridView1.Hide();
            UpdateForm update = new UpdateForm();
            update.Visible = true;
            this.Hide();

        }

        private void deleteStudent(object sender, EventArgs e)
        {
            dataGridView1.Hide();
            DeleteForm delete = new DeleteForm();
            delete.Visible = true;
            this.Hide();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.Data;

namespace SchoolRegistry
{
    class DBConnect
    {
        private MySqlConnection conn;

        public DBConnect()
        {
            initialize();
        }
        private void initialize()
        {
            string server = "127.0.0.1";
            string database = "studentregister";
            string port = "1234";
            string uid = "root";
            string password = "password";

            string connect = "SERVER=" + server + ";UID=" + uid + ";PORT=" + port 
                + ";PASSWORD=" + password + ";DATABASE=" + database + ";";

            conn = new MySqlConnection(connect);
        }

        public bool openConnection()
        {
            try
            {
                conn.Open();
                return true;
            }
            catch(MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        public bool closeConnection()
        {
            try
            {
                conn.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            return false;
        }

        public DataTable viewData()
        {
            openConnection();
            string query = "select * from students;";
            MySqlCommand comm = new MySqlCommand();
            comm.CommandText = query;
            comm.Connection = conn;
            comm.CommandType = System.Data.CommandType.Text;
            MySqlDataAdapter msda = new MySqlDataAdapter(comm);

            DataTable dt = new DataTable();

            msda.Fill(dt);

            closeConnection();

            return dt;
        }

        public bool insert(string firstName, string lastName, int age, string level, string sex)
        {
            string query = "insert into students (firstName, lastName, age, level, sex) values (@first, @last, @age, @level, @sex);";
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = query;
            cmd.Connection = conn;
            cmd.Parameters.AddWithValue("@first", firstName);
            cmd.Parameters.AddWithValue("@last", lastName);
            cmd.Parameters.AddWithValue("@age", age);
            cmd.Parameters.AddWithValue("@level", level);
            cmd.Parameters.AddWithValue("@sex", sex);

            openConnection();
            int result = cmd.ExecuteNonQuery();
            closeConnection();

            return result == 1;
        }

        public bool update(int id,  string field, string replacement)
        {
            string query = "update students set " + field +"=@replace where reg_id=@id";
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = query;
            cmd.Connection = conn;
            cmd.Parameters.AddWithValue("@replace", replacement);
            cmd.Parameters.AddWithValue("@field", field);
            cmd.Parameters.AddWithValue("@id", id);

            openConnection();
            int result = cmd.ExecuteNonQuery();
            closeConnection();

            return result == 1;
        }

        public bool delete(int id)
        {
            string query = "delete from students where reg_id = @id";
            MySqlCommand cmd = new MySqlCommand();
            cmd.CommandText = query;
            cmd.Connection = conn;
            cmd.Parameters.AddWithValue("@id", id);

            openConnection();
            int result = cmd.ExecuteNonQuery();
            closeConnection();

            return result == 1;
        }
    }
}

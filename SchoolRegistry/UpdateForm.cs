﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolRegistry
{
    public partial class UpdateForm : Form
    {
        DBConnect conn = new DBConnect();

        public UpdateForm()
        {
            InitializeComponent();
        }

        private void updateButton(object sender, EventArgs e)
        {
            int id = (int)idUpDown.Value;
            string field = (string)fieldComboBox.SelectedItem;
            string value = valueTextBox.Text;

            if(conn.update(id, field, value))
            {
                MessageBox.Show("Update Successfull!!!");
                Close();
            }
            else
            {
                MessageBox.Show("Update Failed");
            }
        }

        private void updateClosed(object sender, FormClosedEventArgs e)
        {
            Form1 mainform = new SchoolRegistry.Form1();
            mainform.Visible = true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolRegistry
{
    public partial class DeleteForm : Form
    {
        DBConnect conn = new DBConnect();

        public DeleteForm()
        {
            InitializeComponent();
        }

        private void DeleteButton(object sender, EventArgs e)
        {
            int id = (int)idUpDown.Value;

            if (conn.delete(id))
            {
                MessageBox.Show("Delete Successfull!!!");
                Close();
            }
            else
            {
                MessageBox.Show("Delete Failed");
            }
        }

        private void deleteClosed(object sender, FormClosedEventArgs e)
        {
            Form1 mainform = new SchoolRegistry.Form1();
            mainform.Visible = true;
        }
    }
}

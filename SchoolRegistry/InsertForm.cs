﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchoolRegistry
{
    public partial class InsertForm : Form
    {
        DBConnect conn = new DBConnect();

        public InsertForm()
        {
            InitializeComponent();
        }

        private void InsertForm_Load(object sender, EventArgs e)
        {

        }

        private void CloseInsertForm(object sender, FormClosedEventArgs e)
        {
            Form1 mainform = new SchoolRegistry.Form1();
            mainform.Visible = true;
        }

        private void submit(object sender, EventArgs e)
        {
            if (firstNameTextBox.Text == "" || lastNameTextBox.Text == "" || 
                levelComboBox.Text == "" || sexComboBox.Text == "")
            {
                MessageBox.Show("Must fill all fields");
            }
            else
            {
                string firstName = firstNameTextBox.Text;
                string lastName = lastNameTextBox.Text;
                int age = (int) ageUpDown.Value;
                string level = levelComboBox.Text;
                string sex = sexComboBox.Text;

                if(conn.insert(firstName, lastName, age, level, sex))
                {
                    MessageBox.Show("Insert Successfull");
                    Close();
                }
                else
                {
                    MessageBox.Show("An error occurred");
                }
            }
        }
    }
}
